
var dssv = [];
var baseURL="https://62f8b78ae0564480352bf7a7.mockapi.io";
var batloading=function(){
  document.getElementById("loading").style.display="flex";
}
var tatloading=function(){
  document.getElementById("loading").style.display="none";
}
var renderdssv= function(list){
var contentHTML="";
list.forEach(function(item){
    var tr_content=`
    <tr>
    <td>${item.ma}</td>
    <td>${item.ten}</td>
    <td>${item.email}</td>
    <td><img src="${item.hinhAnh}" style="width:40px" alt="" /></td>
    <td>
    <button  onclick="xoaSinhvien(${item.ma})" class="btn btn-danger">xóa</button>
    <button  onclick="suaSinhvien(${item.ma})" class="btn btn-danger">sửa</button>
    </td>
    </tr>
    `;
    contentHTML+=tr_content;
});
document.getElementById("tbodySinhVien").innerHTML=contentHTML;
}

var renderdssvService=function(){
  batloading();
  axios({
    url:  `${baseURL}/sv`,
    method: "GET",
  })
    .then(function (res) {
      tatloading();
      dssv = res.data;
      console.log("dssv: ", dssv);
      renderdssv(dssv);
    })
    .catch(function (err) {
      tatloading();
      console.log(err);
    });
}
renderdssvService();
function  xoaSinhvien(id){
 batloading();
  axios({
    url:`https://62f8b78ae0564480352bf7a7.mockapi.io/sv/${id}`,
    method: "DELETE",
  })  
  .then(function(res){
    tatloading();
    renderdssvService();
      console.log(res);
  })
  .catch(function (err) {
    tatloading();
    console.log(err);
  });
}

function themSV(){
  var dataform=laythongtintuform();
  batloading();
  axios({
    url:`${baseURL}/sv/`,
    method: "POST",
    data:dataform,
  })  
  .then(function(res){
    tatloading();
    renderdssvService();
      
  })
  .catch(function (err) {
    tatloading();
    console.log(err);
  });
}
function suaSinhvien(id){

  batloading();
  axios({
    url:`${baseURL}/sv/${id}`,
    method:"GET",
  })
  .then(function(res){
    tatloading();
    showthongtinlenform(res.data);
  })
  .catch(function (err) {
    tatloading();
    console.log(err);
  });
}
function capNhatSV(){
var id= document.getElementById("txtMaSV").value;
   var dataform=laythongtintuform();
  batloading();
  axios({
    url:`${baseURL}/sv/${id}`,
    method:"PUT",
    data:dataform,
  })
  .then(function(res){
    tatloading();
   renderdssvService();
  })
  .catch(function (err) {
    tatloading();
    console.log(err);
  });
}
function searchsv(){
  var arr_search=[];
  var input_name=document.getElementById("txtSearch").value;
   if(input_name==""){
renderdssvService()
   }else{
    for (var i=0;i<dssv.length;i++){
      if(dssv[i].ten.indexOf(input_name)===0){
        arr_search.push(dssv[i]);
      }
     }
     renderdssv(arr_search);
   }
}